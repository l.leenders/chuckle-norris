import * as React from 'react';
import { CSSTransition, TransitionGroup } from 'react-transition-group'
import axios, { AxiosResponse } from 'axios';
import store from 'store';
import utils from '../utils/utils';

import Intro from './Intro';
import Joke from './Joke';

import './App.scss';

export interface Props {

}

export interface State {
  favourites: Joke[],
  fetchingJoke: boolean,
  initialState?: State,
  jokes: Joke[],
  timer: boolean,
  timerInProgress: boolean,
  timerStatus: string,
  uiState: string
}

class App extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    const favourites: Joke[] = store.get('favourites') ? store.get('favourites') : [];
    const uiState: string = store.get('uiState') ? store.get('uiState') : 'intro';
    const initialState: State = {
      favourites: [],
      fetchingJoke: false,
      jokes: [],
      timer: false,
      timerInProgress: false,
      timerStatus: 'off',
      uiState: 'intro'
    };

    this.state = { ...initialState, ...{ favourites, initialState, uiState } };
  }

  endIntro = () => {
    this.setState({ uiState: 'randomJokes' });
    this.getRandomJokes();
  }

  getRandomJokes = async () => {
    this.setState({ uiState: 'randomJokes' });

    const result: AxiosResponse = await axios({
      method: 'get',
      url: `https://api.icndb.com/jokes/random/10?escape=javascript`,
    });

    const jokes = result.data.value;

    jokes.map((joke: Joke) => {
      const fav = this.state.favourites.filter((fav: Joke) => fav.id === joke.id && fav.favourite);

      joke.favourite = fav.length ? true : false;

      return joke;
    });

    this.setState({ jokes });
  }

  showFavouriteJokes = () => {
    this.setState({ jokes: [], uiState: 'favouriteJokes' });
    store.set('uiState', 'favouriteJokes');
  }

  favouriteTimer = async () => {
    if (!this.state.timerInProgress) {
      this.setState({ timerInProgress: true, fetchingJoke: true });

      await utils.delay(5000);

      while (this.state.favourites.length < 10 && this.state.timerInProgress) {
        const result: AxiosResponse = await axios({
          method: 'get',
          url: `https://api.icndb.com/jokes/random?escape=javascript`,
        });

        result.data.value.favourite = true;

        const favIndex = this.state.favourites.findIndex((el: Joke) => el.id === result.data.value.id);

        if (favIndex === -1) {
          this.state.favourites.push(result.data.value);
        }

        if (this.state.favourites.length > 9) {
          this.setState({ timerInProgress: false });
        }

        this.setState({ favourites: this.state.favourites, fetchingJoke: false })
        store.set('favourites', this.state.favourites);

        await utils.delay(200);

        if (this.state.favourites.length < 10) {
          this.setState({ fetchingJoke: true });
        }

        await utils.delay(5000);


      }
    } else {
      this.setState({ timerInProgress: false, fetchingJoke: false });
    }
  }

  toggleFavourite = async (joke: Joke) => {
    const { jokes, favourites } = this.state;
    const jokeIndex = jokes.findIndex((el: Joke) => el.id === joke.id);
    const favIndex = favourites.findIndex((el: Joke) => el.id === joke.id);

    if (joke.favourite && favIndex > -1) {
      joke.favourite = false;
      favourites.splice(favIndex, 1);
    } else if (favourites.length < 10 && favIndex === -1) {
      joke.favourite = true;
      favourites.push(joke);
    }

    if (jokeIndex > -1) {
      jokes[jokeIndex] = joke;
    }

    this.setState({ jokes, favourites });
    store.set('favourites', favourites);
  }

  clearCache = () => {
    this.setState(this.state.initialState!);

    store.clearAll();
  }

  render() {
    return (
      <React.Fragment>
        {this.state.uiState === 'intro' &&
          <Intro endIntro={this.endIntro} />
        }

        {this.state.uiState === 'randomJokes' &&
          <div className="jokes-container">
            <h2>Here are some random Chuck Norris jokes!</h2>
            <p>Favourite jokes {this.state.favourites.length}/10</p>
            <div className="joke-list">
              <TransitionGroup component={null}>
                {this.state.jokes.map((joke: Joke) => (
                  <CSSTransition key={joke.id} timeout={200} classNames="fade">
                    <Joke key={joke.id} joke={joke} toggleFavourite={this.toggleFavourite} />
                  </CSSTransition>
                ))}
              </TransitionGroup>

              <div className="btn-container">
                <button
                  className="btn shuffle-btn"
                  onClick={this.getRandomJokes}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="shuffle-icon" width="477.88px" height="477.88px" viewBox="0 0 477.88 477.88"><path d="M472.897 124.269a.892.892 0 01-.03-.031l-.017.017-68.267-68.267c-6.78-6.548-17.584-6.36-24.132.42-6.388 6.614-6.388 17.099 0 23.713l39.151 39.151h-95.334c-65.948.075-119.391 53.518-119.467 119.467-.056 47.105-38.228 85.277-85.333 85.333h-102.4C7.641 324.072 0 331.713 0 341.139s7.641 17.067 17.067 17.067h102.4c65.948-.075 119.391-53.518 119.467-119.467.056-47.105 38.228-85.277 85.333-85.333h95.334l-39.134 39.134c-6.78 6.548-6.968 17.353-.419 24.132 6.548 6.78 17.353 6.968 24.132.419.142-.137.282-.277.419-.419l68.267-68.267c6.674-6.657 6.687-17.463.031-24.136z" /><path d="M472.897 329.069l-.03-.03-.017.017-68.267-68.267c-6.78-6.548-17.584-6.36-24.132.42-6.388 6.614-6.388 17.099 0 23.712l39.151 39.151h-95.334a85.209 85.209 0 01-56.9-21.726c-7.081-6.222-17.864-5.525-24.086 1.555-6.14 6.988-5.553 17.605 1.319 23.874a119.28 119.28 0 0079.667 30.43h95.334l-39.134 39.134c-6.78 6.548-6.968 17.352-.42 24.132 6.548 6.78 17.352 6.968 24.132.42.142-.138.282-.277.42-.42l68.267-68.267c6.673-6.656 6.686-17.462.03-24.135zM199.134 149.702a119.28 119.28 0 00-79.667-30.43h-102.4C7.641 119.272 0 126.913 0 136.339s7.641 17.067 17.067 17.067h102.4a85.209 85.209 0 0156.9 21.726c7.081 6.222 17.864 5.525 24.086-1.555 6.14-6.989 5.553-17.606-1.319-23.875z" /></svg>
                </button>

                <button
                  className="btn favourites-btn"
                  onClick={this.showFavouriteJokes}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="favourite-icon" width="26" height="24" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z" /></svg>
                </button>
              </div>
            </div>
          </div>
        }

        {this.state.uiState === 'favouriteJokes' &&
          <div className="jokes-container">
            <h2>{this.state.favourites.length ? `Favourite jokes (${this.state.favourites.length}/10):` : 'Add some jokes!'}</h2>
            <div className="joke-list">
              <TransitionGroup component={null}>
                {this.state.favourites.map((joke: Joke) => (
                  <CSSTransition key={joke.id} timeout={200} classNames="fade">
                    <Joke joke={joke} toggleFavourite={this.toggleFavourite} />
                  </CSSTransition>
                ))}
              </TransitionGroup>

              <div className="btn-container">
                <button
                  className={"btn favourite-timer-btn" + (this.state.timerInProgress ? " in-progress" : "") + (this.state.fetchingJoke ? " progress-filling" : "")}
                  onClick={this.favouriteTimer}
                  disabled={this.state.favourites.length > 9}>
                  <svg className="progress-bar">
                    <circle cx="40" cy="40" r="40"></circle>
                    <circle cx="40" cy="40" r="40"></circle>
                  </svg>
                  <svg xmlns="http://www.w3.org/2000/svg" className="plus-icon" width="426.66667pt" height="426.66667pt" viewBox="0 0 426.66667 426.66667"><path d="m405.332031 192h-170.664062v-170.667969c0-11.773437-9.558594-21.332031-21.335938-21.332031-11.773437 0-21.332031 9.558594-21.332031 21.332031v170.667969h-170.667969c-11.773437 0-21.332031 9.558594-21.332031 21.332031 0 11.777344 9.558594 21.335938 21.332031 21.335938h170.667969v170.664062c0 11.777344 9.558594 21.335938 21.332031 21.335938 11.777344 0 21.335938-9.558594 21.335938-21.335938v-170.664062h170.664062c11.777344 0 21.335938-9.558594 21.335938-21.335938 0-11.773437-9.558594-21.332031-21.335938-21.332031zm0 0" /></svg>
                </button>

                <button
                  className="btn shuffle-btn"
                  onClick={this.getRandomJokes}>
                  <svg xmlns="http://www.w3.org/2000/svg" className="shuffle-icon" width="477.88px" height="477.88px" viewBox="0 0 477.88 477.88"><path d="M472.897 124.269a.892.892 0 01-.03-.031l-.017.017-68.267-68.267c-6.78-6.548-17.584-6.36-24.132.42-6.388 6.614-6.388 17.099 0 23.713l39.151 39.151h-95.334c-65.948.075-119.391 53.518-119.467 119.467-.056 47.105-38.228 85.277-85.333 85.333h-102.4C7.641 324.072 0 331.713 0 341.139s7.641 17.067 17.067 17.067h102.4c65.948-.075 119.391-53.518 119.467-119.467.056-47.105 38.228-85.277 85.333-85.333h95.334l-39.134 39.134c-6.78 6.548-6.968 17.353-.419 24.132 6.548 6.78 17.353 6.968 24.132.419.142-.137.282-.277.419-.419l68.267-68.267c6.674-6.657 6.687-17.463.031-24.136z" /><path d="M472.897 329.069l-.03-.03-.017.017-68.267-68.267c-6.78-6.548-17.584-6.36-24.132.42-6.388 6.614-6.388 17.099 0 23.712l39.151 39.151h-95.334a85.209 85.209 0 01-56.9-21.726c-7.081-6.222-17.864-5.525-24.086 1.555-6.14 6.988-5.553 17.605 1.319 23.874a119.28 119.28 0 0079.667 30.43h95.334l-39.134 39.134c-6.78 6.548-6.968 17.352-.42 24.132 6.548 6.78 17.352 6.968 24.132.42.142-.138.282-.277.42-.42l68.267-68.267c6.673-6.656 6.686-17.462.03-24.135zM199.134 149.702a119.28 119.28 0 00-79.667-30.43h-102.4C7.641 119.272 0 126.913 0 136.339s7.641 17.067 17.067 17.067h102.4a85.209 85.209 0 0156.9 21.726c7.081 6.222 17.864 5.525 24.086-1.555 6.14-6.989 5.553-17.606-1.319-23.875z" /></svg>
                </button>
              </div>
            </div>
          </div>
        }

        {
          this.state.uiState !== 'intro' &&
          <button onClick={this.clearCache} className="btn clear-cache-btn">Clear cache.</button>
        }
      </React.Fragment >
    );
  }
}

export default App;