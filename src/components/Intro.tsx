import * as React from 'react';
import store from 'store';
import utils from '../utils/utils';

export interface Props {
  endIntro: Function
}

export interface State {
  containerClass: string
}

class Intro extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      containerClass: 'intro-container'
    };
  }

  endIntro = async () => {
    this.setState({ containerClass: 'intro-container open' });
    store.set('uiState', 'favouriteJokes');
    await utils.delay(500);
    this.props.endIntro();
  }

  render() {
    return (
      <div className={this.state.containerClass}>
        <img className="logo" src="chuckle-norris.png" alt="" />
        <div className="intro-content">
          <h1>Welcome to Chuckle Norris!</h1>
          <p>It seems like you're new here, let's start with some jokes.</p>
          <button onClick={this.endIntro} className="btn intro-button">Make me Chuckle!</button>
        </div>
      </div>
    );
  }
}

export default Intro;