import * as React from 'react';

export interface Props {
  joke: Joke,
  toggleFavourite: Function
}

class Joke extends React.Component<Props> {
  id: number;
  joke: string;
  favourite: boolean;

  faveBtnClasses() {
    let classes = 'toggle-favourite ';

    classes += this.props.joke.favourite ? 'active' : '';

    return classes;
  }

  render() {
    return (
      <div className="joke-item" key={this.props.joke.id}>
        <span className="clearfix">{this.props.joke.joke}</span>
        <button
          className={this.faveBtnClasses()}
          onClick={() => this.props.toggleFavourite(this.props.joke)}>
          <svg xmlns="http://www.w3.org/2000/svg" width="26" height="24" viewBox="0 0 24 24"><path d="M12 4.248c-3.148-5.402-12-3.825-12 2.944 0 4.661 5.571 9.427 12 15.808 6.43-6.381 12-11.147 12-15.808 0-6.792-8.875-8.306-12-2.944z" /></svg>
        </button>
      </div>
    );
  }
}

export default Joke;