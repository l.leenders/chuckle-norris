const utils = {
  delay: (ms: number) => new Promise(res => setTimeout(res, ms))
};

export default utils;